# Microsoft Developer Studio Generated NMAKE File, Based on TestPluginTCale.dsp
!IF "$(CFG)" == ""
CFG=TestPluginTCale - Win32 Debug
!MESSAGE No configuration specified. Defaulting to TestPluginTCale - Win32 Debug.
!ENDIF 

!IF "$(CFG)" != "TestPluginTCale - Win32 Release" && "$(CFG)" != "TestPluginTCale - Win32 Debug"
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "TestPluginTCale.mak" CFG="TestPluginTCale - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "TestPluginTCale - Win32 Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "TestPluginTCale - Win32 Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE 
!ERROR An invalid configuration is specified.
!ENDIF 

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE 
NULL=nul
!ENDIF 

!IF  "$(CFG)" == "TestPluginTCale - Win32 Release"

OUTDIR=.\Release
INTDIR=.\Release
# Begin Custom Macros
OutDir=.\Release
# End Custom Macros

ALL : "$(OUTDIR)\TestPluginTCale.dll"


CLEAN :
	-@erase "$(INTDIR)\StdAfx.obj"
	-@erase "$(INTDIR)\TCalcFuncSets.obj"
	-@erase "$(INTDIR)\TestPluginTCale.obj"
	-@erase "$(INTDIR)\TestPluginTCale.pch"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(OUTDIR)\TestPluginTCale.dll"
	-@erase "$(OUTDIR)\TestPluginTCale.exp"
	-@erase "$(OUTDIR)\TestPluginTCale.lib"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP=cl.exe
CPP_PROJ=/nologo /MT /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "TESTPLUGINTCALE_EXPORTS" /Fp"$(INTDIR)\TestPluginTCale.pch" /Yu"stdafx.h" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

MTL=midl.exe
MTL_PROJ=/nologo /D "NDEBUG" /mktyplib203 /win32 
RSC=rc.exe
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\TestPluginTCale.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /dll /incremental:no /pdb:"$(OUTDIR)\TestPluginTCale.pdb" /machine:I386 /out:"$(OUTDIR)\TestPluginTCale.dll" /implib:"$(OUTDIR)\TestPluginTCale.lib" 
LINK32_OBJS= \
	"$(INTDIR)\StdAfx.obj" \
	"$(INTDIR)\TCalcFuncSets.obj" \
	"$(INTDIR)\TestPluginTCale.obj"

"$(OUTDIR)\TestPluginTCale.dll" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ELSEIF  "$(CFG)" == "TestPluginTCale - Win32 Debug"

OUTDIR=.\Debug
INTDIR=.\Debug
# Begin Custom Macros
OutDir=.\Debug
# End Custom Macros

ALL : "$(OUTDIR)\TestPluginTCale.dll" "$(OUTDIR)\TestPluginTCale.bsc"


CLEAN :
	-@erase "$(INTDIR)\StdAfx.obj"
	-@erase "$(INTDIR)\StdAfx.sbr"
	-@erase "$(INTDIR)\TCalcFuncSets.obj"
	-@erase "$(INTDIR)\TCalcFuncSets.sbr"
	-@erase "$(INTDIR)\TestPluginTCale.obj"
	-@erase "$(INTDIR)\TestPluginTCale.pch"
	-@erase "$(INTDIR)\TestPluginTCale.sbr"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(INTDIR)\vc60.pdb"
	-@erase "$(OUTDIR)\TestPluginTCale.bsc"
	-@erase "$(OUTDIR)\TestPluginTCale.dll"
	-@erase "$(OUTDIR)\TestPluginTCale.exp"
	-@erase "$(OUTDIR)\TestPluginTCale.ilk"
	-@erase "$(OUTDIR)\TestPluginTCale.lib"
	-@erase "$(OUTDIR)\TestPluginTCale.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP=cl.exe
CPP_PROJ=/nologo /MTd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "TESTPLUGINTCALE_EXPORTS" /FR"$(INTDIR)\\" /Fp"$(INTDIR)\TestPluginTCale.pch" /Yu"stdafx.h" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /GZ /c 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

MTL=midl.exe
MTL_PROJ=/nologo /D "_DEBUG" /mktyplib203 /win32 
RSC=rc.exe
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\TestPluginTCale.bsc" 
BSC32_SBRS= \
	"$(INTDIR)\StdAfx.sbr" \
	"$(INTDIR)\TCalcFuncSets.sbr" \
	"$(INTDIR)\TestPluginTCale.sbr"

"$(OUTDIR)\TestPluginTCale.bsc" : "$(OUTDIR)" $(BSC32_SBRS)
    $(BSC32) @<<
  $(BSC32_FLAGS) $(BSC32_SBRS)
<<

LINK32=link.exe
LINK32_FLAGS=kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /dll /incremental:yes /pdb:"$(OUTDIR)\TestPluginTCale.pdb" /debug /machine:I386 /out:"$(OUTDIR)\TestPluginTCale.dll" /implib:"$(OUTDIR)\TestPluginTCale.lib" /pdbtype:sept 
LINK32_OBJS= \
	"$(INTDIR)\StdAfx.obj" \
	"$(INTDIR)\TCalcFuncSets.obj" \
	"$(INTDIR)\TestPluginTCale.obj"

"$(OUTDIR)\TestPluginTCale.dll" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ENDIF 


!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("TestPluginTCale.dep")
!INCLUDE "TestPluginTCale.dep"
!ELSE 
!MESSAGE Warning: cannot find "TestPluginTCale.dep"
!ENDIF 
!ENDIF 


!IF "$(CFG)" == "TestPluginTCale - Win32 Release" || "$(CFG)" == "TestPluginTCale - Win32 Debug"
SOURCE=.\StdAfx.cpp

!IF  "$(CFG)" == "TestPluginTCale - Win32 Release"

CPP_SWITCHES=/nologo /MT /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "TESTPLUGINTCALE_EXPORTS" /Fp"$(INTDIR)\TestPluginTCale.pch" /Yc"stdafx.h" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 

"$(INTDIR)\StdAfx.obj"	"$(INTDIR)\TestPluginTCale.pch" : $(SOURCE) "$(INTDIR)"
	$(CPP) @<<
  $(CPP_SWITCHES) $(SOURCE)
<<


!ELSEIF  "$(CFG)" == "TestPluginTCale - Win32 Debug"

CPP_SWITCHES=/nologo /MTd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "TESTPLUGINTCALE_EXPORTS" /FR"$(INTDIR)\\" /Fp"$(INTDIR)\TestPluginTCale.pch" /Yc"stdafx.h" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /GZ /c 

"$(INTDIR)\StdAfx.obj"	"$(INTDIR)\StdAfx.sbr"	"$(INTDIR)\TestPluginTCale.pch" : $(SOURCE) "$(INTDIR)"
	$(CPP) @<<
  $(CPP_SWITCHES) $(SOURCE)
<<


!ENDIF 

SOURCE=.\TCalcFuncSets.cpp

!IF  "$(CFG)" == "TestPluginTCale - Win32 Release"


"$(INTDIR)\TCalcFuncSets.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\TestPluginTCale.pch"


!ELSEIF  "$(CFG)" == "TestPluginTCale - Win32 Debug"


"$(INTDIR)\TCalcFuncSets.obj"	"$(INTDIR)\TCalcFuncSets.sbr" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\TestPluginTCale.pch"


!ENDIF 

SOURCE=.\TestPluginTCale.cpp

!IF  "$(CFG)" == "TestPluginTCale - Win32 Release"


"$(INTDIR)\TestPluginTCale.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\TestPluginTCale.pch"


!ELSEIF  "$(CFG)" == "TestPluginTCale - Win32 Debug"


"$(INTDIR)\TestPluginTCale.obj"	"$(INTDIR)\TestPluginTCale.sbr" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\TestPluginTCale.pch"


!ENDIF 


!ENDIF 

